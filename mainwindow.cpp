/*
 *  SPDX-FileCopyrightText: 2021 Tanmay Chavan <earendil01tc@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QGraphicsScene>
#include <QPainterPath>
#include <QPen>
#include <QBrush>
#include "kispathclipper.h"
#include "painterpath.h"
#include "painterpath_p.h"
#include <QWheelEvent>
#include <QGraphicsView>

#include "kisnumericalengine.h"
#include "kisintersectionfinder.h"
#include <QGraphicsSceneMouseEvent>

#include "kisbooleanoperations.h"





MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setMouseTracking(true);
    std::cout.precision(10);
}

MainWindow::~MainWindow()
{
    delete ui;
}


QPainterPath kisUnite(QPainterPath p1, QPainterPath p2){
    KisPathClipper clipper(p1, p2);
    return clipper.clip(KisPathClipper::BoolOr);
}

QPainterPath kisSubtract(QPainterPath p1, QPainterPath p2){
    KisPathClipper clipper(p1, p2);
    return clipper.clip(KisPathClipper::BoolSub);
}

QPainterPath kisIntersect(QPainterPath p1, QPainterPath p2){
    KisPathClipper clipper(p1, p2);
    return clipper.clip(KisPathClipper::BoolAnd);
}

void MainWindow::on_graphicsView_rubberBandChanged(const QRect &viewportRect, const QPointF &fromScenePoint, const QPointF &toScenePoint){

}

void MainWindow::wheelEvent(QWheelEvent *event)
    {
        if (event->modifiers() & Qt::ControlModifier) {
            // zoom
            const QGraphicsView::ViewportAnchor anchor = ui->graphicsView->transformationAnchor();
            ui->graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
            int angle = event->angleDelta().y();
            qreal factor;
            if (angle > 0) {
                factor = 1.1;
            } else {
                factor = 0.9;
            }
            ui->graphicsView->scale(factor, factor);
            ui->graphicsView->setTransformationAnchor(anchor);
        }
//        else {
//            ui->graphicsView->wheelEvent(event);
//        }
    }

// for curve-curve intersection
void MainWindow::on_pushButton_clicked()
{
    scene.clear();
    ui->graphicsView->setScene(&scene);

    QPainterPath qpp;

    QPointF cp0(20, 20); //(4, 1);
    QPointF cp1(40, 260); //(8, 4);
    QPointF cp2(60, -160); //(6, 7);
    QPointF cp3(80,80); //(2, 6);



//    QPointF cba(5, 25); //{ 3,3 };
//    QPointF cbb(300, 35); //{ 8,1 };
//    QPointF cbc(-180, 60); //{ 12,2 };
//    QPointF cbd(100, 70); //{ 11,5 };

//    QPointF cp0(-40, 20); //(4, 1);
//    QPointF cp1(600, 200); //(8, 4);
//    QPointF cp2(-200,200); //(6, 7);
//    QPointF cp3(120, 20); //(2, 6);



    QPointF cba(-20, 25); //{ 3,3 };
    QPointF cbb(300, 35); //{ 8,1 };
    QPointF cbc(-180, 60); //{ 12,2 };
    QPointF cbd(100, 70); //{ 11,5 };


    CubicBezier cb1 {cp0,cp1,cp2,cp3};
    CubicBezier cb2 {cba, cbb, cbc, cbd};

    QPainterPath cp;
    QPainterPath cpp;
    QPainterPath overLimits;

    cp.addEllipse(cp0,1,1);
    cp.addEllipse(cp1,1,1);
    cp.addEllipse(cp2,1,1);
    cp.addEllipse(cp3,1,1);


    cpp.addEllipse(cba,1,1);
    cpp.addEllipse(cbb,1,1);
    cpp.addEllipse(cbc,1,1);
    cpp.addEllipse(cbd,1,1);

//    for (int i = 0; i < 400; i++) {
//        overLimits.addEllipse(QPointF(cb1.getParametricX().evaluate( 0 - (i * 0.01)), cb1.getParametricY().evaluate(0- (i * 0.01))),1,1);
//    }

    KisIntersectionFinder KIF;
    QVector<KisClippingVertex> intersectionPoints =  KIF.intersectionPoint(cb1, cb2);

    QPainterPath intersectionPointsPaths;

    Q_FOREACH(KisClippingVertex p, intersectionPoints) {

        intersectionPointsPaths.addEllipse(p.point,1,1);
//        std::cout << "QPointF( " << p.x() << ", " << p.y() << " ), ";

    }


    QPainterPath c1;
    c1.moveTo(cp0);
    c1.cubicTo(cp1, cp2, cp3);

    QPainterPath c2;
    c2.moveTo(cba);
    c2.cubicTo(cbb, cbc, cbd);

    QPen pen;
    pen.setWidthF(1);
    pen.setColor(QColor("teal"));

    QPen pen2;
    pen2.setWidthF(1);
    pen2.setColor(QColor("pink"));

    QPen pen3;
    pen3.setWidthF(1);
    pen3.setColor(QColor("yellow"));

    QPen pen4;
    pen4.setWidthF(2);
    pen4.setColor(QColor("red"));

    QPen pen5;
    pen5.setWidthF(2);
    pen5.setColor(QColor("green"));

    QPen pen6;
    pen6.setWidthF(50);
    pen6.setColor(QColor("black"));

    scene.addPath(c1,pen);
    scene.addPath(c2,pen2);
    scene.addPath(intersectionPointsPaths, pen3);
    scene.addPath(cp, pen4);
    scene.addPath(cpp, pen5);


//    scene.addPath(overLimits, pen6);
    ui->graphicsView->repaint();



}



// for line-curve intersection
void MainWindow::on_pushButton_2_clicked()
{
    scene.clear();
    ui->graphicsView->setScene(&scene);

    QPainterPath qpp;

    // 45 100   45 69.62433876100000418318814   69.62433876100000418318814 45   100 45

    QPointF cp0(45,100); //(4, 1);
    QPointF cp1(45, 69.62); //(8, 4);
    QPointF cp2(69.62,45); //(6, 7);
    QPointF cp3(100, 45); //(2, 6);

    CubicBezier cb1 {cp3, cp2, cp1, cp0};

    // curveTo 100 45   130.3756612390000100276666 45   155 69.62433876100000418318814   155 100

    QPointF cpa(100, 45); //{ 3,3 };
    QPointF cpb(130.3756, 45); //{ 8,1 };
    QPointF cpc(155, 69.6243); //{ 12,2 };
    QPointF cpd(155, 100); //{ 11,5 };

    CubicBezier cb2 {cpd, cpc, cpb, cpa};

    QPainterPath line;

    double yC = 45.5;

    QLineF qL(QPointF(50,yC), QPointF(150, yC));
    Line l1(qL);

    QLineF qL2(QPointF(50,50), QPointF(60, 100));
    Line l2(qL2);

//    QVector<double> gg = l1.findRoots(cb1);
//    QVector<double> gg2 = l2.findRoots(cb1);

    KisIntersectionFinder KIF;

    QVector<KisClippingVertex> intPoints = KIF.intersectionPoint(l1, cb1);
    QVector<KisClippingVertex> intPoints2 = KIF.intersectionPoint(l2, cb1);

    QVector<KisClippingVertex> intPoints3 = KIF.intersectionPoint(l1, cb2);

    QPainterPath intersectionPointsPaths;

    Q_FOREACH(KisClippingVertex p, intPoints) {
        intersectionPointsPaths.addEllipse(p.point,1,1);
        std::cout.precision(10);
        std::cout << "QPointF( " << p.point.x() << ", " << p.point.y() << " ), ";

    }

    QPainterPath intersectionPointsPaths2;

    Q_FOREACH(KisClippingVertex p, intPoints2) {
        intersectionPointsPaths2.addEllipse(p.point,1,1);
    }

    QPainterPath intersectionPointsPaths3;

    Q_FOREACH(KisClippingVertex p, intPoints3) {
        intersectionPointsPaths3.addEllipse(p.point,1,1);
    }

    QPainterPath boundingbox;

    QRectF bb = cb1.boundingBox();
    boundingbox.addRect(bb);

    QPainterPath c1;
    c1.moveTo(cp0);
    c1.cubicTo(cp1, cp2, cp3);

    QPainterPath cbbb2;
    c1.moveTo(cpa);
    c1.cubicTo(cpb, cpc, cpd);

    QPainterPath c2;
    c2.moveTo(QPointF(qL.p1()));
    c2.lineTo(qL.p2());

    QPainterPath c3;
    c3.moveTo(qL2.p1()); //QPointF(40,20), QPointF(40, 180)
    c3.lineTo(qL2.p2());

    QPen pen;
    pen.setWidthF(1);
    pen.setColor(QColor("teal"));

    QPen pen2;
    pen2.setWidthF(1);
    pen2.setColor(QColor("pink"));

    QPen pen3;
    pen3.setWidthF(1);
    pen3.setColor(QColor("black"));

    QPen pen4;
    pen4.setWidthF(1.5);
    pen4.setColor(QColor("red"));

    QPen pen5;
    pen5.setWidthF(1);
    pen5.setColor(QColor("cyan"));

//    scene.addPath(c1,pen);
//    scene.addPath(cbbb2 , pen);
//    scene.addPath(c2,pen2);
//    scene.addPath(c3,pen2);
//    scene.addPath(intersectionPointsPaths, pen3);
//    scene.addPath(intersectionPointsPaths2, pen4);
//    scene.addPath(intersectionPointsPaths3, pen3);
//    scene.addPath(boundingbox,pen5);

    QPainterPath ellipse, eles;
    ellipse.addEllipse(QPointF(150, 100), 90,70);

    scene.addPath(ellipse, pen);
    for (int i = 0; i < ellipse.elementCount(); i++ ) {

//        if (ellipse.elementAt(i).type == QPainterPath::CurveToDataElement){
//            continue;
//        }
        std::cout << "***---***---***---***" << ellipse.elementAt(i).type << std::endl;
        eles.addEllipse(ellipse.elementAt(i),1,1);
    }
    scene.addPath(eles, pen4);


    ui->graphicsView->repaint();

}

void printType(QPainterPath mp) {
    for(int i = 0; i < mp.elementCount(); i++) {
        QPainterPath::Element e = mp.elementAt(i);
        int type = e.type;

        std::cout << type << ", ";
        //    if(type == 0) {
        //        std::cout << "moveTo" << std::endl;
        //    }

        //    else if(type == 1) {
        //        std::cout << "lineTo" << std::endl;
        //    }

        //    else if(type == 2) {
        //        std::cout << "curveTo" << std::endl;
        //    }

        //    else if(type == 3) {
        //        std::cout << "curveToData" << std::endl;
        //    }


    }
}


// for QPainterPath intersections
void MainWindow::on_pushButton_3_clicked()
{

    auto start = high_resolution_clock::now ();
    scene.clear();
    ui->graphicsView->setScene(&scene);

    QPainterPath qpp;

    QPainterPath horiRect;
    horiRect.addRoundedRect(50,50,100,100, 20, 20);
    //horiRect.addRect(40,50,150,100);
//    horiRect.addEllipse(QPointF(70,70),120,50);

    QPainterPath ellipse;
    ellipse.addEllipse(QPointF(110, 100),79.79, 55);
//    ellipse.addRect(50,18,100,200);

    KisIntersectionFinder KIF(ellipse, horiRect);

    QVector<KisClippingVertex> result = KIF.findAllIntersections();

    QPainterPath markers2;

//    std::cout << "path int points:\n";
    Q_FOREACH(KisClippingVertex pt, result) {
//        std::cout.precision(10);
//        std::cout << "QPointF( " << pt.x() << ", " << pt.y() << " ), ";
        markers2.addEllipse(pt.point,1, 1);
    }

//    QPainterPath markers;

//    for (int i = 0; i < horiRect.elementCount(); i++) {
//        markers.addEllipse(horiRect.elementAt(i),1,1);
//    }

    QPainterPath bruh1;
    QPainterPath bruh2;

    QPointF cp1(30,60);
    QPointF cp2(60,40);
    QPointF cp3(80,40);
    QPointF cp4(120,10);


    Line l1(QPointF(20,50), QPointF(120, 50));
    CubicBezier cb(cp1, cp2, cp3, cp4);



    bruh1.moveTo(20,50);
    bruh1.lineTo(120, 50);

    bruh2.moveTo(cp1);
    bruh2.cubicTo( cp2, cp3, cp4);

    std::cout << "horiRect" << std::endl;
    printType(horiRect);

//    for(int i = 0; i < KIF.subjectShape.size(); i++) {
//    buildingBlock e = KIF.subjectShape.at(i);
//    }



    std::cout << "ellipse" << std::endl;
    printType(ellipse);

    KisIntersectionFinder kif2(bruh1, bruh2);
//    QVector<QPointF> res2 = kif2.findAllIntersections();



    auto stop = high_resolution_clock::now ();

    auto duration = duration_cast<microseconds>( stop - start );

    std::cout << "total time: " << duration.count () << " microseconds" << std::endl;

//    Q_FOREACH(QPointF pt, res2) {
//        markers.addEllipse(pt,1, 1);
//    }


    auto start1 = high_resolution_clock::now ();

    QPainterPath p1;
    QPainterPath p2;
    p1.addRoundedRect(50,50,100,100, 20, 20);
    //horiRect.addEllipse(QPointF(70,70),120,50);

    p2.addEllipse(QPointF(110, 100),79.79, 55);

    QPainterPath res;
    res = p1.united(p2);

    auto stop1 = high_resolution_clock::now ();

    auto duration1 = duration_cast<microseconds>( stop1 - start1 );

    std::cout << "+++++++++ total time: " << duration1.count () << " microseconds" << std::endl;

    QPen pen;
    pen.setWidthF(0.3);
    pen.setColor(QColor("teal"));

    QPen pen2;
    pen2.setWidthF(0.3);
    pen2.setColor(QColor("orange"));

    QPen pen3;
    pen3.setWidthF(1);
    pen3.setColor(QColor("brown"));

    QPen pen4;
    pen4.setWidthF(1);
    pen4.setColor(QColor("red"));

    QPen pen5;
    pen5.setWidthF(0.5);
    pen5.setColor(QColor("cyan"));

    /*enum ElementType {
        MoveToElement,
        LineToElement,
        CurveToElement,
        CurveToDataElement
    };                      */

 //   scene.addPath(qpp);

//    scene.addPath(bruh1,pen);
//    scene.addPath(bruh2,pen2);
//    scene.addPath(markers, pen3);

    QPainterPath bb;
    QVector<BuildingBlock> b= KIF.getSubjectShape();
    b[1].getCurveTo().computeBoundingBox();
    bb.addRect(b[1].getCurveTo().boundingBox());

//    scene.addPath(horiRect,pen);
//    scene.addPath(ellipse,pen2);
//    scene.addPath(markers2, pen3);
//    scene.addPath(bb, pen5);

    QPainterPath path1;
    QPainterPath path2;
    path1.addEllipse(QPointF(330, 100), 30, 120);
    path2.addRoundedRect(347,50,200,100,20,20);

    path1 -= path2;

    scene.addPath(path1, pen2);
    ui->graphicsView->repaint();
}



bool comparePts(QPointF first, QPointF second) {

    return qAbs(first.x() - second.x()) < 10e-6 && qAbs(first.y() - second.y()) < 10e-6;
}


QPainterPath substituteCurves() {

}



// clipped shapes
void MainWindow::on_pushButton_4_clicked()
{

    scene.clear();
    ui->graphicsView->setScene(&scene);

    QPainterPath roundedRect, rect;
    QPainterPath ellipse;
    QPainterPath markers;

//    roundedRect.addEllipse(QPointF(447, 100), 120, 55); //addRoundedRect(240,50,200,100,20,20); //roundedRect.addEllipse(QPointF(150, 100), 70,90);//
//    ellipse.addRoundedRect(347,50,200,100,20,20); //addEllipse(QPointF(340, 100), 120, 30); // //
//    rect.addRect(230,55,200,85);

//    roundedRect.addEllipse(QPointF(460, 100), 30, 120); //addEllipse(QPointF(347, 50), 30, 120); //addRoundedRect(240,50,200,100,20,20); //roundedRect.addEllipse(QPointF(150, 100), 70,90);//
//    ellipse.addRoundedRect(360,50,200,100,20,20); //addEllipse(QPointF(220, 100), 30, 120); // //
//    rect.addRect(230,55,200,85);

//    roundedRect.addRect(50,50,200,100);
//    ellipse.addRect(50,60, 100, 200);

    roundedRect.addEllipse(QPointF(330, 100), 30, 120); //addRoundedRect(240,50,200,100,20,20); //roundedRect.addEllipse(QPointF(150, 100), 70,90);//
        ellipse.addRoundedRect(347,50,200,100,20,20);


    KisIntersectionFinder KIF(roundedRect, ellipse);
    QVector<KisClippingVertex> intPoints = KIF.findAllIntersections();

    Q_FOREACH( KisClippingVertex v, intPoints ) {

        markers.addEllipse(v.point, 3, 3);
    }

    KIF.processShapes();

    QPainterPath sub = KIF.subjectShapeToPath();
    QPainterPath clip = KIF.clipShapeToPath();

//    KisPathClipper clipper(sub, clip);
    QPainterPath res = sub | clip;

    QPainterPath substitutedRes = KIF.resubstituteCurves(res);
    std::cout << "sizeeeeee: " << substitutedRes.elementCount() << std::endl;

    QPen pen;
    pen.setWidthF(3);
    pen.setColor(QColor("orange"));

    QPen pen2;
    pen2.setWidthF(0.3);
    pen2.setColor(QColor("fuschia"));


    QPen penVar;
    penVar.setWidthF(1);

        scene.addPath(substitutedRes, pen);
    int rcolor = 0;
    for (int i = 0; i < substitutedRes.elementCount() ; i++ ) {

        QPainterPath pth;


        if (i == 0) {
            penVar.setWidth(1);
            penVar.setColor(QColor("green"));
        }
        else if (i == 1) {
            penVar.setWidth(1);
            penVar.setColor(QColor("green"));
        }
        else {
            penVar.setWidth(1);
            penVar.setColor(QColor("green")); //penVar.setColor(QColor((rcolor++ * 90) % 255,255,255));
        }

        pth.addEllipse(substitutedRes.elementAt(i),1,1); //pth.addEllipse(sub.elementAt(i),1 + (0.1 * i),1 + (0.1 * i));
        scene.addPath(pth, penVar);
        pth.clear();
    }
    ui->graphicsView->repaint();
}

void MainWindow::on_pushButton_5_clicked()
{
    scene.clear();
    ui->graphicsView->setScene(&scene);


//    QPainterPath roundedRect, rect;
//    QPainterPath ellipse;

//    roundedRect.addEllipse(QPointF(480, 100), 120, 30); //addEllipse(QPointF(347, 50), 30, 120); //addRoundedRect(240,50,200,100,20,20); //roundedRect.addEllipse(QPointF(150, 100), 70,90);//
//    ellipse.addEllipse(QPointF(540, 100), 30, 120); // //
//    rect.addRect(230,55,200,85);


//    KisIntersectionFinder KIF(roundedRect, ellipse);
//    QVector<KisClippingVertex> intPoints = KIF.findAllIntersections();

//    KIF.processShapes();

//    QPainterPath sub = KIF.subjectShapeToPath();
//    QPainterPath clip = KIF.clipShapeToPath();

////    KisPathClipper clipper(sub, clip);
//    QPainterPath res = sub | clip;

//    QPainterPath substitutedRes = KIF.resubstituteCurves(res);

////    std::cout << "sizeEeEeEeE: " << substitutedRes.elementCount() << std::endl;

    QPainterPath ellipse;
    QPainterPath roundedRect;

    ellipse.addEllipse(QPointF(460, 80), 30, 120);
    roundedRect.addRoundedRect(360, 50, 200, 100, 20, 20);

    KisBooleanOperations booleanOpsHandler;

    QPainterPath res = booleanOpsHandler.subtract(ellipse, roundedRect);
//    res.addPath(rect1);
//    res.addPath(rect2);

    std::cout << "SIZE: *__*" << res.elementCount() << std::endl;

    scene.addPath(res);

//    QCOMPARE(res.elementCount(), 15);

    ui->graphicsView->repaint();

}
